<?php

namespace System;

use System\User as User;
use MVC\Session as Session;

class MyController extends \MVC\Controller
{
    private $forceValidate = false;
    
    public function __construct()
    {
        if( User::online() ){
            $this->breadcrumb[] = array( 'title' => 'Início', 'url' => url.'/'.strtolower(User::online()->getGroupObj()->getMode()) );
        }
        @$GLOBALS['config']['define']['url_mode'] = url_mode;
        @$GLOBALS['config']['define']['url_module'] = url_module;
    }


    protected function json( $render = array(), $validate = false )
    {
        header('Content-Type: text/html; charset=UTF-8');

        #Tratamento
        if( $validate || $this->forceValidate ){
            $errors = $this->error();
            if( $errors ){
                $render['status'] = false;
                $render['msg'] = (implode('<br/>', $errors));
                $this->error(null);
            }
        }
        
        die(json_encode($render));
    }
    
    protected function view($template = "", $options = array())
    {
        if( !empty($this->breadcrumb) ){
            $GLOBALS['view']['breadcrumb'] = $this->breadcrumb ;
        }
        
        $View = parent::view($options);
        
        if( $template ){
            return $View->setTemplate($template);
    }
    
        return $View;
    }
    
    protected function session()
    {
        $args = func_get_args();
        
        if( !array_key_exists(0, $args) ){
            return Session::get();
        }elseif( !array_key_exists(1, $args) ){
            return Session::get($args[0]);
        }else{
            return Session::set($args[0], $args[1]);
        }
    }
    
    protected function post( $name = null )
    {
        if( $name ){
            return !empty($_POST[$name]) ? $_POST[$name] : null;
        }else{
            return $_GET;
        }
    }
    
    protected function get( $name = null )
    {
        if( $name ){
            return !empty($_GET[$name]) ? $_GET[$name] : null;
        }else{
            return $_GET;
        }
    }
    
    protected function files( $name = null )
    {
        if( $name ){
            return !empty($_FILES[$name]) ? $_FILES[$name] : null;
        }else{
            return $_FILES;
        }
    }
    
    protected function error()
    {
        $args = func_get_args();
        
        if( !array_key_exists(0, $args) ){
            return _getErrors();
        }else{
            if( $args[0] ){
                $this->forceValidate = true;
                _setError($args[0]);
                return $this;
            }else{
                return _clearErrors();
            }
        }
    }
    
    protected function success()
    {
        $args = func_get_args();
        
        if( !array_key_exists(0, $args) ){
            return _getSuccess();
        }else{
            if( $args[0] ){
                _setSuccess($args[0]);
                return $this;
            }else{
                return _clearSuccess();
            }
        }
    }
    
}