<?php

namespace Core\Model;

use MVC\Model as Model;

class Group extends Model{
    
    protected $ID;
    protected $name;
    protected $mode;
    protected $status;
    protected $defaultPage;
    
    protected $defaultUrl;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setName( $name ){
        $this->name = $name;
        return $this;
    }

    public function getName(){
        return $this->name;
    }

    public function setMode( $mode ){
        $this->mode = $mode;
        return $this;
    }

    public function getMode(){
        return $this->mode;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setDefaultPage( $defaultPage ){
        $this->defaultPage = $defaultPage;
        return $this;
    }

    public function getDefaultPage(){
        return $this->defaultPage;
    }

    public function setDefaultUrl( $defaultUrl ){
        $this->defaultUrl = $defaultUrl;
        return $this;
    }

    public function getDefaultUrl(){
        return $this->defaultUrl;
    }
    
    protected function triggerAfterLoad(){
        if( $this->defaultPage ){
            $default   = explode("|", $this->defaultPage);
            $mode      = strtolower($default[0]);
            $module    = $default[1];
            $submodule = $default[2];
            $this->defaultUrl = "/{$mode}";
            if( $submodule !== "index"){
                $this->defaultUrl .= "/{$module}/{$submodule}";
            }elseif( $module !== "index" ){
                $this->defaultUrl .= "/{$module}";
            }
        }
    }
    
    public static function listAll(){
        $sql = "select id from hazo_group where mode <> 'S' order by mode, name";
        $res = _query($sql);
        $data = array();
        foreach( $res as $row ){
            $data[] = Group::getByID($row['id']);
        }
        return count($data) > 0 ? $data : null;
    }
    
}