<?php

namespace Core\System;

class File{

    protected $path;
    protected $files;
    protected $folders;

    public function __construct( $path = null, $load = true ){
        if( $path ){
            $this->setPath($path);
            if( $load ){
                $this->loadPath();
            }
        }
        return $this;
    }

    public function getFiles($extension=null, $returFullPath = false){
        if( $extension ){
            $file = array();
            if( $this->files ){
                foreach( $this->files as $file ){
                    if( strtoupper(substr($file, strlen($file) - strlen($extension) )) === strtoupper($extension) ){
                        $files[] = $file;
                    }
                }
            }
            $this->files = @$files;
        }
        if( $returFullPath && $this->files ){
            foreach( $this->files as $ind => $file ){
                $this->files[$ind] = realpath($this->path.$file);
            }
        }
        return $this->files;
    }

    public function getFolders(){
        return $this->folders;
    }

    public function setPath( $path ){
        if( substr($path, -1, 1) != '/' ){
            $path .= "/";
        }
        $this->path = $path;
    }

    public function loadPath(){
        @$cursor = opendir($this->path);
        while( @$itensName = readdir($cursor) ){
            $itens[] = $itensName;
        }
        @sort($itens);
        if( $itens ){
            foreach( $itens as $listar ){
                if( $listar != "." && $listar != ".." ){
                    if( file_exists($this->path.$listar) && is_file($this->path.$listar) ){
                        $this->files[] = $listar;
                    }else{
                        $this->folders[] = $listar;
                    }
                }
            }
        }
        return $this;
    }
    
    public function loadFile( $filename ){
        if( file_exists($this->path.$filename) ){
            return file_get_contents($this->path.$filename);
        }else{
            return false;
        }
    }

    public function saveFile( $filename, $content ){
        $f = fopen($this->path.$filename, 'w');
        fwrite($f, $content);
        return fclose($f);
    }

    public function deleteFile( $filename ){
        if( file_exists($this->path.$filename) ){
            return unlink($this->path.$filename);
        }else{
            return false;
        }
    }

    public function renameFile( $filename, $newFilename ){
        if( file_exists($this->path.$filename) ){
            return rename($this->path.$filename, $this->path.$newFilename);
        }else{
            return false;
        }
    }

    public function createFolder( $foldername, $mode = 0777 ){
        return mkdir($this->path.$foldername, $mode);
    }

    public function deleteFolder( $foldername ){
        if( is_dir($this->path.$foldername) ){
            return rmdir($this->path.$foldername);
        }else{
            return false;
        }
    }

    /**
     * Função para limpar e/ou excluir um diretório.
     * @param string $directory
     * @param boolean $empty
     * @return boolean
     * @link http://www.php.net/manual/en/function.rmdir.php#98499
     */
    public function deleteAll( $directory, $empty = false ){
        if( substr($directory, -1) == "/" ){
            $directory = substr($directory, 0, -1);
        }

        if( !file_exists($directory) || !is_dir($directory) ){
            return false;
        }elseif( !is_readable($directory) ){
            return false;
        }else{
            $directoryHandle = opendir($directory);

            while( $contents = readdir($directoryHandle) ){
                if( $contents != '.' && $contents != '..' ){
                    $path = $directory."/".$contents;

                    if( is_dir($path) ){
                        deleteAll($path);
                    }else{
                        unlink($path);
                    }
                }
            }

            closedir($directoryHandle);

            if( $empty == false ){
                if( !rmdir($directory) ){
                    return false;
                }
            }

            return true;
        }
    }

    public function loadJsonFile( $filename ){
        $s = $this->loadFile($filename);
        return Format::jsonFormat($s, "out");
    }

    public function saveJsonFile( $filename, $content ){
        $f = fopen($this->path.$filename, 'w');
        fwrite($f, Format::jsonFormat($content, "in"));
        return fclose($f);
    }

}