<?php

namespace Core\System;

class JsonObject{

    public static function jsonToObject( $json, $returnObject = true ){

        $array = is_array($json) ? $json : @json_decode($json, true);

        if( isset($array['__class_name__']) ){
            $Obj = new $array['__class_name__']();
        }else{
            return $json;
        }

        foreach( $array as $ind => $row ){
            if( is_array($row) ){
                foreach( $array[$ind] as $ind2 => $row2 ){

                    if( is_array($row2) && isset($row2['__class_name__']) ){
                        $tmp = &$Obj->$ind;
                        $tmp[$ind2] = self::jsonToObject($row2);
                    }elseif( is_array($row2) ){
                        foreach( $row2 as $ind3 => $row3 ){
                            $Obj->$ind[$ind2][$ind3] = self::jsonToObject($row3, false);
                        }
                    }else{
                        $Obj->$ind = $row;
                    }
                }
            }else{
                $Obj->$ind = $row;
            }
        }
        return $Obj;
    }

    public static function objectToJson( $Obj, $returnJson = true ){

        if( !is_object($Obj) ){
            return self::indentJson(json_encode($Obj));
        }

        $atts = get_object_vars($Obj);
        $array['__class_name__'] = get_class($Obj);

        foreach( $atts as $u => $t ){
            if( is_array($t) ){
                foreach( $Obj->$u as $u2 => $t2 ){
                    $array[$u][$u2] = self::objectToJson($t2, false);
                }
            }else{
                $array[$u] = $Obj->$u;
            }
        }

        return $returnJson ? self::indentJson(json_encode($array)) : $array;
    }

    public static function indentJson( $json ){
        $result = '';
        $pos = 0;
        $strLen = strlen($json);
        $indentStr = "    ";
        $newLine = "\n";
        $prevChar = '';
        $outOfQuotes = true;
        for( $i = 0; $i <= $strLen; $i++ ):
            $char = substr($json, $i, 1);
            if( $char == '"' && $prevChar != '\\' ):
                $outOfQuotes = !$outOfQuotes;
            elseif( ($char == '}' || $char == ']') && $outOfQuotes ):
                $result .= $newLine;
                $pos--;
                for( $j = 0; $j < $pos; $j++ ):
                    $result .= $indentStr;
                endfor;
            endif;
            $result .= $char;
            if( ($char == ',' || $char == '{' || $char == '[') && $outOfQuotes ):
                $result .= $newLine;
                if( $char == '{' || $char == '[' ):
                    $pos++;
                endif;

                for( $j = 0; $j < $pos; $j++ ):
                    $result .= $indentStr;
                endfor;
            endif;
            $prevChar = $char;
        endfor;
        return $result;
    }

}