<?php

namespace Lib;

class File{

    public static function delete( $path )
    {
        if( is_file($path) ){
            chmod($path, 0777);
            return unlink($path);
        }else{
            $handle = opendir($path);
            while( $file = readdir($handle) ){
                if( $file != '..' && $file != '.' ){
                    if( is_file($path.DS.$file) ){
                        unlink($path.DS.$file);
                    }else{
                        self::delete($path.DS.$file);
                    }
                }
            }
            closedir($handle);
            chmod($path, 0777);
            return rmdir($path);
        }
    }

}