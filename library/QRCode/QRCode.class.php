<?php

namespace Lib\QRCode;

class QRCode{
    
    private $obj;
    
    public function __construct(){
        include_once("class/Lib/QRCode/Origin/BarcodeQR.php");
        $this->obj = new \BarcodeQR();
    }
    
    public function setURL($url){
        $this->obj->url("{$url}"); 
    }
    
    public function getContentImage(){
        return $this->obj->draw(150,null,true);
    }
    
}