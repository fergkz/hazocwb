$(function() {
    var slider = $('.bxslider').bxSlider({
        mode: 'fade',
        controls: false,
        pager: true

    });
    $('#slide-show .bt-nav-dir').click(function() {
        slider.goToNextSlide();
        return false;
    });
    $('#slide-show .bt-nav-esq').click(function() {
        slider.goToPrevSlide();
        return false;
    });
    /*
     var sliderAtual = 1;
     var totalSlider = $("#slide-show .slides-content .slide").size();
     var larg = $("#slide-show .slides").width() + 2;
     
     $("#slide-show .bt-nav-esq").click(function() {
     navSlider("#slide-show .slides-content", "-", larg);
     sliderAtual++;
     });
     $("#slide-show .bt-nav-dir").click(function() {
     navSlider("#slide-show .slides-content", "+", larg);
     sliderAtual--;
     console.log(sliderAtual);
     });
     
     
     function navSlider(classe, direcao, larg) {
     if(sliderAtual == 0){
     
     }
     
     if (sliderAtual < totalSlider ) {
     $(classe).animate({"left": direcao + "=" + larg}, 700); 
     }
     
     
     }
     */
});

