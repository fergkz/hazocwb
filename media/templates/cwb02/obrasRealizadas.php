<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="css/reset.css" type="text/css" rel="stylesheet"/>
        <link href="css/estrutura.css" type="text/css" rel="stylesheet"/>
        <link href="css/obrasRealizadas.css" type="text/css" rel="stylesheet"/>
        <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:300,400|Inconsolata&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/jquery.bxslider.min.js"></script>
        <script src="js/banner.js"></script>
        <!--[if IE]><script src="js/html5shiv.js"></script><![endif]-->
    </head>
    <body>
        <!--[if IE]><div>UTILIZE O CHROME</div><![endif]-->
    <main>
        <header id="cabecalho">
            <div id="bannerTopo"><a href="index.php"><img src="img/logo.png" alt="CWB Brazilians"/></a></div>

            <nav>
                <ul>
                    <li><a href="index.php" title="Projetos">Projetos</a></li>
                    <li class="selecionado"><a href="obrasRealizadas.php" title="Obras realizadas">Obras realizadas</a></li>
                    <li><a href="contato.php" title="Contato">Contato</a></li>
                    <li><a href="empresa.php" title="A Empresa">A Empresa</a></li>
                </ul>
            </nav>
        </header>

        <div id="conteudo">
            <h2>Obras realizadas</h2>
            <div id="slide-show">
                <div class="bt-nav bt-nav-esq"></div>
                <ul  class="bxslider">
                    <li>
                        <img src="img/projeto-casa.jpg"/>
                    </li>
                    <li>slide 02</li>
                    <li>slide 03</li>
                    <li>slide 04</li>
                </ul>
                <div class="bt-nav bt-nav-dir"></div>
            </div>
        </div>


       
        <footer id="rodape">
            <dl>
                <dt>Telefones :</dt>
                <dd class="oper-tim">+55 (41) 9601-5967</dd>
                <dd class="oper-vivo">+55 (41) 9159-8191</dd>
                <dd class="oper-oi">+55 (41) 8486-0542</dd>
                <dd class="oper-claro">+55 (41) 8835-9640</dd>
            </dl>
            <p class="copyright">Copyright &copy; 2014 - CWB Brazilians - Todos os Direitos Reservados.</p>
        </footer>
    </main>
</body>
</html>