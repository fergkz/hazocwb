/** Plugin Jquery **/
        (function(jQuery) {
            jQuery.fn.tabs = function(settings) {   
                
                settings = jQuery.extend({
                    default:"",
                }, settings);
                /*****************************************************/
                jQuery(this).each(function(){
                    var tabs = jQuery(this);
                    var tabsId = tabs.attr("id");
                    tabs.find("div").hide().first().show();
                    tabs.find("ul li").first().addClass("selected");
                    
                    tabs.find("ul li a").each(function(index, value){
                        var padrao = new RegExp("("+tabsId+")-*");
                        if(padrao.test($(this).attr("href"))){
                            $(this).click(function(){
                                tabs.find("div").hide();
                                tabs.find("ul li").removeClass("selected");
                                $(this).parent().addClass("selected");
                                 tabs.find($(this).attr("href")).show();
                                return false;
                            });
                        }
                    });
                    
                });
            }  
        })(jQuery);
        
        /***************************************************/
        
        
/** Plugin Jquery **/
        (function(jQuery) {
            jQuery.fn.slideShowGP = function(settings) {   
                
                settings = jQuery.extend({
                    itens:"li",
                    paginacaoBlc:"",
                    paginacaoAtivo:"",
                    idBanner:"",
                    posBanner:2,
                    tituloBanner:"pubBanner",
                    automatico:true,
                    intervaloTempo:7000
                }, settings);
                /*****************************************************/
                jQuery(this).each(function(){
                    var blocoitems=jQuery(this);
                   // qtdeitens=blocoitems.children(settings.itens).length;
                    blocoitems.children(settings.itens+":not(:eq(0))").hide();
                    if(settings.idBanner.length>1&&jQuery(settings.idBanner).length > 0){
                        gerarBanner(blocoitems,settings);
                    }else{
                       settings.idBanner=""; 
                    }
                    
                    
                    
                    jQuery(settings.paginacaoBlc).empty();
                    if(settings.automatico){
                        blocoitems.data('intervalo',setTimeout(function(){proximo(blocoitems,settings);},settings.intervaloTempo));
                    }
                    blocoitems.children(settings.itens).each(function(index) {
                        
                        var li=jQuery("<li></li>");
                        var a=jQuery("<a></a>")
                        .text(index+1)
                        .bind('click',function(e){
                            if(settings.automatico){
                                clearInterval(blocoitems.data('intervalo'));
                            }
                            var itemAtual;
                            if(jQuery(settings.paginacaoBlc).prop('tagName').toLowerCase()=='ul'){
                                itemAtual= jQuery(this).parent().index();
                            }else{
                                itemAtual= jQuery(this).index();
                            }
                            
                            mudarItem(blocoitems,settings,itemAtual);
                            if(settings.automatico){
                                blocoitems.data('intervalo',setTimeout(function(){proximo(blocoitems,settings);},settings.intervaloTempo));
                            }
                        });
                        if(jQuery(settings.paginacaoBlc).prop('tagName').toLowerCase()=='ul'){
                            a.appendTo(li);
                            li.appendTo(settings.paginacaoBlc);
                        }else{
                            a.appendTo(settings.paginacaoBlc);
                        }
                    });  
                    jQuery(settings.paginacaoBlc+" a:eq(0)").addClass(settings.paginacaoAtivo);
                });

                /***************************************************/
                function gerarBanner(blocoitems,settings){
                    var divBanner=jQuery("<div></div>")
                        .attr("id",blocoitems.attr("id")+"Banner");
                    var spanConteiner=jQuery("<span></span>").attr("id",settings.tituloBanner);
                    var spanTitulo=jQuery("<span></span>");
                    var aFechar=jQuery("<a></a>");
                    var cssBanner={
                        
                        "position":"absolute",
                        "z-index": 999
                    };
                    divBanner.hide();
                    divBanner.css(cssBanner);    
                    spanTitulo
                        .html("Publicidade")
                        .appendTo(spanConteiner);
                    aFechar
                        .html("Fechar")
                        .bind('click',function(e){
                            blocoitems.data("banner","off");
                            fecharBanner(blocoitems,settings);
                        })
                        .appendTo(spanConteiner);
                    
                    jQuery(settings.idBanner).css(cssBanner); 
                    jQuery(settings.idBanner).prepend(spanConteiner);
           

                    blocoitems.prepend(jQuery(divBanner));
                    blocoitems.data("banner","on");
                      
                }                
                
                abrirBanner=function(blocoitems,settings){
                    var proxBanner=jQuery(settings.itens+":visible").index();
                    if(proxBanner==settings.posBanner){
                        jQuery(settings.idBanner).fadeIn(800);
                        jQuery(settings.idBanner).data('intervalo',setTimeout(function(){fecharBanner(blocoitems,settings);},settings.intervaloTempo));
                    }
                };
                
                fecharBanner=function(blocoitems,settings){
                    var idBanner="#"+blocoitems.attr("id")+"Banner"; 
                    clearInterval(jQuery(settings.idBanner).data('intervalo'));
                    
                    var proxBanner=jQuery(settings.itens+":visible").index();
                    mudarItem(blocoitems,settings,proxBanner);
                    jQuery(settings.idBanner).fadeOut(800);
                    if(settings.automatico){
                        blocoitems.data('intervalo',setTimeout(function(){proximo(blocoitems,settings);},settings.intervaloTempo));
                    }
                    //blocoitems.data("banner","off");
                    
                };
                
                mudarItem = function (blocoitems,settings,item){ 
                    var qtdeitens = blocoitems.children(settings.itens).length;
                    if(item>=qtdeitens){
                        item=0;
                    }
                    blocoitems.children(settings.itens).hide();
                    blocoitems.children(settings.itens+":eq("+item+")").fadeIn(800);
                    
                    jQuery(settings.paginacaoBlc+" a").removeClass(settings.paginacaoAtivo);
                    jQuery(settings.paginacaoBlc+" a:eq("+item+")").addClass(settings.paginacaoAtivo);
                };

                proximo = function(blocoitems,settings){
                    var itemAtual;
                    if(jQuery(settings.paginacaoBlc).prop('tagName').toLowerCase()=='ul'){
                        itemAtual= jQuery(settings.paginacaoBlc+" li").children("."+settings.paginacaoAtivo).parent().index();
                    }else{
                        itemAtual= jQuery(settings.paginacaoBlc).children("."+settings.paginacaoAtivo).index();
                    }
                    var qtdeitens = blocoitems.children(settings.itens).length;
                    
                    itemAtual++;
                    if(itemAtual>=qtdeitens){
                        itemAtual=0;
                    }
                    
                    if(settings.idBanner.length>1&& blocoitems.data("banner")=="on"&&itemAtual==settings.posBanner){
                        abrirBanner(blocoitems,settings);
                    }else{
                        if(settings.automatico){
                            mudarItem(blocoitems,settings,itemAtual);
                            blocoitems.data('intervalo',setTimeout(function(){proximo(blocoitems,settings);},settings.intervaloTempo));
                        }
                    }
                };
               
            }  
        })(jQuery);
        
        /***************************************************/
