function hideError()
{
    $('#msgError').html('');
    $('#msgError').hide();
}

function showError(msg)
{
    div = $('<div/>').html(msg);
    $('#msgError').fadeIn().append(div);
}

function showSuccess(msg)
{
    div = $('<div/>').html(msg);
    $('#msgSuccess').fadeIn().append(div);
}

function showLoad()
{
    $('.btn-submit').each(function(){
        $(this).data('content-value', $(this).val());
        $(this).data('content-html', $(this).html());
        
        $(this).html('Enviando. Aguarde...');
        $(this).val('Enviando. Aguarde...');
        $(this).attr('disabled', true)
    });
}

function hideLoad()
{
    $('.btn-submit').each(function(){
        $(this).html($(this).data('content-html'));
        $(this).val($(this).data('content-value'));
        $(this).attr('disabled', false)
    });
}

$().ready(function(){
    
    $('.form-ajax').ajaxForm({
        dataType: 'json',
        type: 'POST',
        beforeSend: function(){
            showLoad();
        },
        success: function( data ){
            if( data.status ){
                window.location = data.redirect;
            }else{
                hideError();
                showError(data.msg);
                hideLoad();
            }
        },
        error: function(){
            hideError();
            showError("Falha ao enviar formulário. Tente novamente mais tarde.");
            hideLoad();
        }
    });
    
    $( 'textarea.ckeditor-simples' ).ckeditor({
        toolbar: [
            { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ] },
            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'HorizontalRule', 'SpecialChar' ] },
            { name: 'tools', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','Font','FontSize','Maximize' ] },
        ]
    });
    
});