<?php

namespace Model\CWB;

class Anexo extends \System\MyModel
{    
    public static $daoTable = "pro_anexo";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'              => 'id',
        'projetoID'       => 'projeto_id',
        'modeloID'        => 'modelo_id',
        'obraRealizadaID' => 'obra_realizada_id',
        'fileUrl'         => 'file_url',
        'filePath'        => 'file_path',
        'fileName'        => 'file_name',
        'fileType'        => 'file_type',
        'tipo'            => 'tipo',
        'principal'       => 'principal',
        'language'        => 'language',
        'conteudoTXT'     => 'conteudo_txt',
        'ordenacao'       => 'ordenacao'
    );

    protected $ID;
    protected $projetoID;
    protected $modeloID;
    protected $obraRealizadaID;
    protected $fileUrl;
    protected $filePath;
    protected $fileName;
    protected $fileType;
    protected $tipo;
    protected $principal;
    protected $language;
    protected $conteudoTXT;
    protected $ordenacao;
    
    protected function triggerBeforeSave()
    {
//        debug($this);
    }
    
}