<?php

namespace Model\CWB;

use Core\System\Mail as Mail;
use Model\Sistema\Usuario as Usuario;

class Contato extends \System\MyModel
{    
    public static $daoTable = "contato";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'                => 'id',
        'nome'              => 'nome',
        'telefone'          => 'telefone',
        'email'             => 'email',
        'localConstrucao'   => 'local_construcao',
        'mensagem'          => 'mensagem',
        'projetoID'         => 'projeto_id',
        'dataEnvio'         => 'data_envio'
    );

    protected $ID;
    protected $nome;
    protected $telefone;
    protected $email;
    protected $localConstrucao;
    protected $mensagem;
    protected $projetoID;
    protected $dataEnvio;
    
    public function getProjetoObj()
    {
        $Projeto = new Projeto($this->projetoID);
        return $Projeto->getID() ? $Projeto : null;
    }
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction === 'I' ){
            $this->dataEnvio = null;
            //$this->dataEnvio = date('Y-m-d H:i:s');
        }
    }
    
    protected function triggerAfterSave()
    {
        if( !$this->send() ){
            $this->raise("Não foi possível enviar o contato. Tente novamente mais tarde");
        }
    }
    
    private function send()
    {
        $Mail = new Mail();
        
        $listaUsuarios = Usuario::getList();
        
        foreach( $listaUsuarios['rows'] as $Usuario ){
            $Mail->setAddress($Usuario->getEmail(), $Usuario->getNome());
        }
        
        $render = array(
            "Destinatario" => 'Administrador',
            "Nome" =>         $this->nome,
            "Telefone" =>     $this->telefone,
            "Email" =>        $this->email,
            "Local" =>        $this->localConstrucao,
            "Projeto" =>      $this->getProjetoObj(),
            "Mensagem" =>     $this->mensagem
        );
        
        $Mail->setMessageTemplate("templates/email/contato-projeto.twig", $render);
        $Mail->setSubject("Contato Projeto - brazilianscasas.com.br");
        
        return $Mail->send();
    }
    
}