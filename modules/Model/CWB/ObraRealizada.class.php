<?php

namespace Model\CWB;

use Model\CWB\Anexo as Anexo;

use Lib\WideImage\Image as Image;
use Lib\File as File;

class ObraRealizada extends \System\MyModel
{    
    public static $daoTable = "pro_obra_realizada";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'                  => 'id',
        'titulo'              => 'titulo',
        'descricao'           => 'descricao'
    );

    protected $ID;
    protected $titulo;
    protected $descricao;
    protected $anexoObj;
    
    private $file;
    private $sizes = array(150,600);
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction === 'D' ){
            $lista = Anexo::getList(array(
                'dao.obra_realizada_id = ?' => $this->ID
            ), null, 0, 1);
            foreach( $lista['rows'] as $Anexo ){
                @File::delete($Anexo->getFilePath());
                $Anexo->save('D');
            }
        }
    }
    
    protected function triggerAfterSave()
    {
        if( $this->daoAction !== 'D' ){
        
            $lista = Anexo::getList(array(
                'dao.obra_realizada_id = ?' => $this->ID
            ), null, 0, 1);

            if( $lista['cont_total'] <= 0 && (empty($this->file) || !$this->file['tmp_name']) ){
                $this->raise("O arquivo informado é inválido");
            }elseif( !empty($this->file) && $this->file['tmp_name'] ){
                $sizes = $this->sizes;

                $path = path.ds.'media'.ds.'file'.ds.'obras';
                if( !file_exists($path) ){
                    mkdir($path, 0777, true);
                    chmod($path, 0777);
                }

                $path = path.ds.'media'.ds.'file'.ds.'obras'.ds.$this->ID;
                if( !file_exists($path) ){
                    mkdir($path, 0777, true);
                    chmod($path, 0777);
                }

                if( $lista['cont_total'] > 0 ){
                    $Anexo = $lista['rows'][0];
                }else{
                    $Anexo = new Anexo();
                }

                $Anexo->setObraRealizadaID($this->ID);
                $Anexo->setFileUrl(url."/media/file/obras/{$this->ID}");
                $Anexo->setFilePath($path);
                $Anexo->setFileName(null);
                $Anexo->setFileType($this->file['type']);
                $Anexo->setTipo('IMG');
                $Anexo->setPrincipal('Y');

                if( $Anexo->save() ){

                    foreach( $sizes as $size ){
                        $Image = Image::loadFromFile($this->file['tmp_name'])->resize($size);

                        $filename = $path.ds.$size.'.png';

                        if( file_exists($filename) ){
                            File::delete($filename);
                        }
                        if( !$Image->saveToFile($filename) ){
                            $this->raise("Não foi possível salvar a imagem. Verifique se o arquivo enviado é uma imagem válida.");
                        }
                        chmod($filename, 0777);
                    }

                }else{
                    $this->raise($Anexo->daoErrorMessage);
                }
            }
        }
    }
    
    public function setFile( $name, $type, $tmp_name, $size )
    {
        $this->file = array( 
            'name' => $name,
            'type' => $type,
            'tmp_name' => $tmp_name,
            'size' => $size
        );
    }
    
    public function getImageUrl($size=600)
    {
        $lista = Anexo::getList(array(
            'dao.obra_realizada_id = ?' => $this->ID
        ), null, 0, 1);
        
        return $lista['rows'][0]->getFileUrl()."/{$size}.png";
        
        debug($lista);
    }
    
}