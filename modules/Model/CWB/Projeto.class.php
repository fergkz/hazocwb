<?php

namespace Model\CWB;

use Model\CWB\Anexo as Anexo;
use Model\CWB\ProjetoModelo as Modelo;

use \Lib\Format as Format;
use Lib\WideImage\Image as Image;
use Lib\File as File;

class Projeto extends \System\MyModel
{    
    public static $daoTable = "pro_projeto";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'                 => 'id',
        'titulo'             => 'titulo',
        'descricao'          => 'descricao',
        'slug'               => 'slug',
        'dataCadastro'       => 'data_cadastro',
        'memorialDescritivo' => 'memorial_descritivo',
        'ordenacao'          => 'ordenacao'
    );

    protected $ID;
    protected $titulo;
    protected $descricao;
    protected $slug;
    protected $dataCadastro;
    protected $memorialDescritivo;
    protected $ordenacao;
    
    private $files = array();
    private $unset_files = array();
    
    public function getDescricao($format='html')
    {
        if( $format === 'text' ){
            return trim(utf8_encode(html_entity_decode( strip_tags($this->descricao) )));
        }else{
            return trim($this->descricao);
        }
    }
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction === 'D' ){
            $modelos = Modelo::getList(array(
                'dao.projeto_id = ?' => $this->ID
            ));
            foreach( $modelos['rows'] as $Obj ){
                $Obj->save('D');
            }
        }
        
        $this->slug = Format::strigToUrl($this->titulo);
        if( $this->daoAction === 'I' ){
            $this->dataCadastro = date('Y-m-d H:i:s');
        }
    }
    
    protected function triggerAfterSave()
    {
        if( count($this->files) > 0 ){
        
            $sizes = array(100,300,500,800);
            
            $path = path.ds.'media'.ds.'file'.ds.'projeto'.ds.$this->ID;
            if( !file_exists($path) ){
                mkdir($path, 0777, true);
                chmod($path, 0777);
            }
            
            $path .= ds.'images';
            if( !file_exists($path) ){
                mkdir($path, 0777);
                chmod($path, 0777);
            }
            
            foreach( $this->files as $row ){

                $dir = md5(time().$row['name']);
                
                $filepath = $path.ds.$dir;
                @File::delete($filepath);
                mkdir($filepath, 0777);
                chmod($filepath, 0777);

                $Anexo = new Anexo();
                $Anexo->setProjetoID($this->ID);
                $Anexo->setFileUrl( url."/media/file/projeto/{$this->ID}/images/{$dir}" );
                $Anexo->setFilePath( $filepath );
                $Anexo->setFileName($dir);
                $Anexo->setFileType($row['type']);
                $Anexo->setTipo('IMG');
                $Anexo->setPrincipal('N');
                
                if( $Anexo->save() ){

                    foreach( $sizes as $size ){
                        $Image = Image::loadFromFile($row['tmp_name']);
                        $Image = $Image->resize($size);

                        if( !$Image->saveToFile($filepath.ds.$size.'.png') ){
                            $this->raise("Não foi possível salvar a imagem do projeto. Verifique se o arquivo enviado é uma imagem válida.");
                        }
                        chmod($filepath.ds.$size.'.png', 0777);
                    }
                    
                }else{
                    $this->raise($Anexo->daoErrorMessage);
                }
                
            }
        }
        
        if( count($this->unset_files) > 0 ){
            foreach( $this->unset_files as $id ){
                $Anexo = new Anexo($id);
                
                if( $Anexo->getProjetoID() !== $this->ID ){
                    $this->raise("O arquivo informado não pertence a este projeto");
                }
                if( !File::delete($Anexo->getFilePath()) ){
                    $this->raise("Não foi possível excluir o caminho {$Anexo->getFilePath()}");
                }
                
                if( !$Anexo->save('D') ){
                    $this->raise($Anexo->daoErrorMessage);
                }
            }
        }
    }
    
    public static function getBySlug( $slug )
    {
        $lista = self::getList(array(
            'dao.slug = ?' => $slug
        ), null, 0, 1);
        
        return $lista['cont_total'] > 0 ? $lista['rows'][0] : false;
    }
    
    public function setFile( $name, $type, $tmp_name, $size )
    {
        $this->files[] = array( 
            'name' => $name,
            'type' => $type,
            'tmp_name' => $tmp_name,
            'size' => $size
        );
    }
    
    
    public function unsetFile( $id )
    {
        $this->unset_files[] = $id;
    }
    
    public function listaImagens($size=100, $limit = null, $showObjs = false)
    {
        $urls = array();
        
        $lista = Anexo::getList(array(
            'dao.projeto_id = ?' => $this->ID,
            'dao.tipo = ?' => 'IMG'
        ), array('fileUrl','ID'), 0, $limit, array('dao.ordenacao asc','dao.id asc'));
        
        foreach( $lista['rows'] as $Obj ){
            if( !$showObjs ){
                $urls[$Obj->getID()] = $Obj->getFileUrl()."/{$size}.png";
            }else{
                $urls[$Obj->getID()] = $Obj;
                $urls[$Obj->getID()]->tmpFileUrl = $Obj->getFileUrl()."/{$size}.png";
            }
        }
        
        return count($urls) > 0 ? $urls : null;
    }
    
    public function getModeloPrincipal()
    {
        $modelos = Modelo::getList(array(
            'dao.projeto_id = ?' => $this->ID
        ), null, 0, 1, array('dao.id desc'));
        return $modelos['rows'][0];
    }
}