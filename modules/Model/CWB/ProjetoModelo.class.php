<?php

namespace Model\CWB;

use Lib\Format as Format;
use Lib\File as File;
use Lib\WideImage\Image as Image;

class ProjetoModelo extends \System\MyModel
{    
    public static $daoTable = "pro_modelo";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'            => 'id',
        'projetoID'     => 'projeto_id',
        'titulo'        => 'titulo',
        'slug'          => 'slug'
    );

    protected $ID;
    protected $projetoID;
    protected $titulo;
    protected $slug;
    
    private $files = array();
    private $unset_files = array();
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction === 'D' ){
            $imgs = Anexo::getList(array(
                'dao.modelo_id = ?' => $this->ID
            ));
            if( $imgs['cont_total'] ){
                foreach( $imgs['rows'] as $Obj ){
                    $this->unsetFile($Obj->getID());
                }
                $this->deleteFiles();
            }
        }
        $this->slug = Format::strigToUrl($this->titulo);
    }
    
    public static function getBySlug( $projeto, $slug )
    {
        $lista = self::getList(array(
            'dao.slug = ?' => $slug,
            'dao.projeto_id = ?' => (is_object($projeto) ? $projeto->getID() : $projeto)
        ), null, 0, 1);
        
        return $lista['cont_total'] > 0 ? $lista['rows'][0] : false;
    }
    
    protected function triggerAfterSave()
    {
        if( count($this->files) > 0 ){
        
            $sizes = array(100,300,500,800);
            
            $path = path.ds.'media'.ds.'file'.ds.'projeto'.ds.$this->projetoID.ds.'modelos';
            if( !file_exists($path) ){
                mkdir($path, 0777, true);
                chmod($path, 0777);
            }
            
            $path .= ds.$this->ID;
            if( !file_exists($path) ){
                mkdir($path, 0777, true);
                chmod($path, 0777);
            }
            
            $path .= ds.'images';
            if( !file_exists($path) ){
                mkdir($path, 0777);
                chmod($path, 0777);
            }
            
            foreach( $this->files as $row ){

                $dir = md5(time().$row['name']);
                
                $filepath = $path.ds.$dir;
                @File::delete($filepath);
                mkdir($filepath, 0777);
                chmod($filepath, 0777);

                $Anexo = new Anexo();
                $Anexo->setModeloID($this->ID);
                $Anexo->setFileUrl( url."/media/file/projeto/{$this->projetoID}/modelos/{$this->ID}/images/{$dir}" );
                $Anexo->setFilePath( $filepath );
                $Anexo->setFileName($dir);
                $Anexo->setFileType($row['type']);
                $Anexo->setTipo('IMG');
                $Anexo->setPrincipal('N');
                
                if( $Anexo->save() ){

                    foreach( $sizes as $size ){
                        $Image = Image::loadFromFile($row['tmp_name']);
                        $Image = $Image->resize($size);

                        if( !$Image->saveToFile($filepath.ds.$size.'.png') ){
                            $this->raise("Não foi possível salvar a imagem do modelo. Verifique se o arquivo enviado é uma imagem válida.");
                        }
                        chmod($filepath.ds.$size.'.png', 0777);
                    }
                    
                }else{
                    $this->raise($Anexo->daoErrorMessage);
                }
                
            }
        }
        
        $this->deleteFiles();
    }
    
    private function deleteFiles()
    {
        if( count($this->unset_files) > 0 ){
            foreach( $this->unset_files as $id ){
                $Anexo = new Anexo($id);
                if( $Anexo->getModeloID() !== $this->ID ){
                    $this->raise("O arquivo informado não pertence a este modelo");
                }
                if( file_exists($Anexo->getFilePath()) && !File::delete($Anexo->getFilePath()) ){
                    $this->raise("Não foi possível excluir o caminho {$Anexo->getFilePath()}");
                }
                
                if( !$Anexo->save('D') ){
                    $this->raise($Anexo->daoErrorMessage);
                }
            }
        }
        $this->unset_files = null;
    }
    
    public function setFile( $name, $type, $tmp_name, $size )
    {
        $this->files[] = array( 
            'name' => $name,
            'type' => $type,
            'tmp_name' => $tmp_name,
            'size' => $size
        );
    }
    
    
    public function unsetFile( $id )
    {
        $this->unset_files[] = $id;
    }
    
    public function listaImagens($size=100)
    {
        $urls = array();
        
        $lista = Anexo::getList(array(
            'dao.modelo_id = ?' => $this->ID,
            'dao.tipo = ?' => 'IMG'
        ), array('fileUrl','ID'));
        
        foreach( $lista['rows'] as $Obj ){
            $urls[$Obj->getID()] = $Obj->getFileUrl()."/{$size}.png";
        }
        
        return count($urls) > 0 ? $urls : null;
    }
    
    public static function getList( $whereColumns = array(), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array(), $join = null, $groupBy = null ){
        
        $order = array_merge(array('dao.titulo'), $order);
        
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join, $groupBy);
    }
}