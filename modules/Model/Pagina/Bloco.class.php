<?php

namespace Model\Pagina;

class Bloco extends \System\MyModel
{    
    public static $daoTable = "pag_param_bloco";
    public static $daoPrimary = array('token' => 'token');
    public static $daoCols = array(
        'token'  => 'token',
        'conteudo'  => 'conteudo'
    );

    protected $token;
    protected $conteudo;
    
    protected function triggerBeforeSave()
    {
        # NULL
    }
    
    public static function get($token)
    {
        $lista = self::getList(array(
            'dao.token = ?' => $token
        ), null, 0, 1);
        
        return $lista['cont_total'] > 0 ? $lista['rows'][0]->getConteudo() : "";
    }
    
}