<?php

namespace Model\Pagina;

class Sample extends \System\MyModel
{    
    public static $daoTable = "nome_tabela";
    public static $daoPrimary = array('atributoID' => 'coluna_id');
    public static $daoCols = array(
        'atributoID'  => 'coluna_id',
        'outraColuna'  => 'outra_coluna'
    );

    protected $atributoID;
    protected $outraColuna;
    
    protected function triggerBeforeSave()
    {
        # NULL
    }
    
}