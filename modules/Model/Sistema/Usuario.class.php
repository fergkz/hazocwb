<?php

namespace Model\Sistema;

use MVC\Session as Session;

class Usuario extends \System\MyModel
{    
    public static $daoTable = "sis_usuario";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'     => 'id',
        'nome'   => 'nome',
        'login'  => 'login',
        'senha'  => 'senha',
        'email'  => 'email'
    );

    protected $ID;
    protected $nome;
    protected $login;
    protected $senha;
    protected $email;
    
    public static function getLogado()
    {
        $online = Session::get('usuario_online');
        return $online ? $online : false;
    }
    
    public static function login($login, $senha)
    {
        self::logout();
        
        $lista = Usuario::getList(array(
            'dao.login = ?' => $login,
            'dao.senha = ?' => md5($senha)
        ), null, 0, 1);
        
        if( $lista['cont_total'] > 0 ){
            Session::set('usuario_online', $lista['rows'][0]);
            return true;
        }else{
            return false;
        }
    }
    
    public static function logout()
    {
        if( self::getLogado() ){
            Session::set('usuario_online', null);
        }
        return true;
    }
    
    
    protected function triggerBeforeSave()
    {
        if( !$this->senha ){
            $this->raise("A senha deve ser informada");
        }
        
        if( $this->daoAction === 'I' ){
            $this->senha = md5($this->senha);
        }
        elseif( $this->daoAction === 'U' ){
            if( $this->senha !== $this->old->senha ){
                $this->senha = md5($this->senha);
            }
        }
    }
    
}