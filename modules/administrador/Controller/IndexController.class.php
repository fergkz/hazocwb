<?php

use Model\Sistema\Usuario as Usuario;

class IndexController extends \System\MyController
{
    public function indexAction()
    {
        if( Usuario::getLogado() ){
            $this->redirect( url_mode."/painel" );
        }else{
            $this->redirect( url_mode."/sessao/login" );
        }
    }
}