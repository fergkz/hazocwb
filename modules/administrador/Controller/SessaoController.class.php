<?php

use Model\Sistema\Usuario as Usuario;

class SessaoController extends \System\MyController
{
    public function loginAction()
    {
        $this->view('sessao/login.twig')->display();
    }
    
    public function entrarAction()
    {
        if( Usuario::login($this->post('login'), $this->post('senha')) ){
            $render['status'] = true;
            $render['redirect'] = url_mode."/painel/projeto/lista";
        }else{
            $render['status'] = false;
            $render['msg'] = "Usuário ou senha inválida";
        }
        
        $this->json($render, true);
    }
    
    public function sairAction()
    {
        Usuario::logout();
        $this->redirect(url);
    }
    
}