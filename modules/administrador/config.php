<?php

use MVC\Session as Session;

$GLOBALS['usuario_online'] = Session::get('usuario_online');

if( $GLOBALS['request']['module'] !== "sessao" && !$GLOBALS['usuario_online'] ){
    header("Location: ".url."/administrador/sessao/login");
}