<?php

use Model\Pagina\Bloco as Bloco;

class ConfiguracoesController extends \System\MyController
{
    private $Usuario;

    /**
     * View
     */
    public function contatoAction()
    {   
        $render = array(
            'tel_oi' => Bloco::get('contato_tel_oi'),
            'tel_tim' => Bloco::get('contato_tel_tim'),
            'tel_vivo' => Bloco::get('contato_tel_vivo'),
            'tel_claro' => Bloco::get('contato_tel_claro'),
        );
        
        $this->view('contato/index.twig')->display($render);
    }
    
    /**
     * Ajax/Json
     */
    public function contatoSalvarAction()
    {        
        foreach( $this->post('bloco') as $token => $conteudo ){
            $Bloco = new Bloco();
            $Bloco->setToken($token);
            $Bloco->setConteudo($conteudo);
            
            if( !$Bloco->save() ){
                $render['status'] = false;
                $render['msg'] = $Bloco->daoErrorMessage;
                $this->json($render);
            }
        }
        
        $render['status'] = true;
        $render['redirect'] = url."/administrador/painel";
        $this->json($render, true);
    }
    
}