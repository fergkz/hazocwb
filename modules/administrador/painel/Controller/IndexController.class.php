<?php

class IndexController extends \System\MyController
{
    private $Usuario;

    /**
     * View
     */
    public function indexAction()
    {
        $render['Usuario'] = $this->Usuario;
        $this->view('index/index.twig')->display($render);
    }
}