<?php

use Model\CWB\ObraRealizada as Obra;

class ObrasRealizadasController extends \System\MyController
{
    /**
     * View
     */
    public function indexAction()
    {   
        $render['obras'] = Obra::getList();
        $this->view('obras-realizadas/lista.twig')->display($render);
    }
    
    /**
     * View
     */
    public function novoAction()
    {
        $this->view('obras-realizadas/cadastro.twig')->display();
    }
    
    /**
     * Ajax/Json
     */
    public function criarAction()
    {
        $render['status'] = false;
        
        $this->setObraData($render);
        
        $this->json($render, true);
    }
    
    /**
     * View
     */
    public function edicaoAction( $obraID )
    {
        $Obra = new Obra($obraID);
        
        if( !$Obra || !$Obra->getID() ){
            return 404;
        }
        
        $render['Obra'] = $Obra;
        
        $this->view('obras-realizadas/cadastro.twig')->display($render);
    }
    
    /**
     * Ajax/Json
     */
    public function alterarAction( $obraID )
    {
        $Obra = new Obra($obraID);
        
        if( !$Obra || !$Obra->getID() ){
            return 404;
        }
        
        $render['status'] = false;
        
        $this->setObraData($render, $Obra);
        
        $this->json($render, true);
    }
    
    /**
     * Private Manipulate
     */
    private function setObraData(&$render, $Obra = null)
    {
        $acao = $Obra ? "U" : "I";
        
        if( $acao === 'I' ){
            $Obra = new Obra();
        }
        $Obra->setTitulo( time().rand(111,999) );
        $Obra->setDescricao( $this->post('descricao') );

        if( !empty($_FILES['file']) && !empty($_FILES['file']['name']) ){
            $Obra->setFile( $_FILES['file']['name'], $_FILES['file']['type'], $_FILES['file']['tmp_name'], $_FILES['file']['size'] );
        }

        if( $Obra->save() ){
            $render['status'] = true;
            $render['redirect'] = url."/administrador/painel/obras-realizadas";
        }else{
            $render['msg'] = $Obra->daoErrorMessage;
        }
    }
    
    /**
     * Ajax/Json
     */
    public function excluirAction( $obraID )
    {
        $render['status'] = false;
        $Obra = new Obra($obraID);
        
        if( !$Obra || !$Obra->getID() ){
            $render['msg'] = "Obra inválida";
            $this->json($render);
        }
        
        if( $Obra->save('D') ){
            $render['status'] = true;
            $render['redirect'] = url."/administrador/painel/obras-realizadas";
        }else{
            $render['msg'] = $Obra->daoErrorMessage;
        }
        
        $this->json($render, true);
    }
    
}