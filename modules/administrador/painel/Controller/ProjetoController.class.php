<?php

use Model\CWB\Projeto as Projeto;
use Model\CWB\ProjetoModelo as Modelo;
use Model\CWB\Anexo as Anexo;

class ProjetoController extends \System\MyController
{
    /**
     * View
     */
    public function listaAction()
    {   
//        $render['projetos'] = Projeto::getList();
        $render['projetos'] = Projeto::getList(array(), null, 0, null, array('dao.ordenacao asc'));
        $this->view('projeto/lista.twig')->display($render);
    }
    
    /**
     * View
     */
    public function novoAction()
    {
        $this->view('projeto/cadastro.twig')->display();
    }
    
    /**
     * Ajax/Json
     */
    public function criarAction()
    {
        $render['status'] = false;
        
        $this->setProjetoData($render);
        
        $this->json($render, true);
    }
    
    /**
     * View
     */
    public function edicaoAction( $slug )
    {
        $Projeto = Projeto::getBySlug($slug);
        
        if( !$Projeto ){
            return 404;
        }
        
        $render['Projeto'] = $Projeto;
        $render['imagens'] = $Projeto->listaImagens();
        
        $this->view('projeto/cadastro.twig')->display($render);
    }
    
    /**
     * Ajax/Json
     */
    public function alterarAction( $slug )
    {
        $Projeto = Projeto::getBySlug($slug);
        
        if( !$Projeto ){
            return 404;
        }
        
        $render['status'] = false;
        
        $this->setProjetoData($render, $Projeto);
        
        $this->json($render, true);
    }
    
    /**
     * View
     */
    public function visualizacaoAction( $slug )
    {
        $Projeto = Projeto::getBySlug($slug);
        
        if( !$Projeto ){
            return 404;
        }

        $render['Projeto'] = $Projeto;
        $render['modelos'] = Modelo::getList(array(
            'dao.projeto_id = ?' => $Projeto->getID()
        ));
        $render['imagens'] = $Projeto->listaImagens(100, null, true);

        $this->view("projeto/visualizacao.twig")->display($render);
    }
    
    /**
     * Private Manipulate
     */
    private function setProjetoData(&$render, $Projeto = null)
    {
        $acao = $Projeto ? "U" : "I";
        
        if( !$this->post('titulo') ){
            $render['msg'] = "O nome do projeto deve ser informado";
        }else{
            
            if( $acao === 'I' ){
                $Projeto = new Projeto();
            }
            $Projeto->setTitulo( $this->post('titulo') );
            $Projeto->setDescricao( $this->post('descricao') );
            $Projeto->setMemorialDescritivo( $this->post('memorial') );

            if( !empty($_FILES['file']) &&  !empty($_FILES['file']['name']) && count($_FILES['file']['name']) > 0 ){
                foreach( $_FILES['file']['name'] as $ind => $name ){
                    if( $name ){
                        $Projeto->setFile( $name, $_FILES['file']['type'][$ind], $_FILES['file']['tmp_name'][$ind], $_FILES['file']['size'][$ind] );
                    }
                }
            }
            
            if( $this->post('file_excluir') && count($this->post('file_excluir')) > 0 ){
                foreach( $this->post('file_excluir') as $id ){
                    $Projeto->unsetFile($id);
                }
            }
            
            if( $Projeto->save() ){
                $render['status'] = true;
                $render['redirect'] = url."/administrador/painel/projeto/visualizacao/".$Projeto->getSlug();
            }else{
                $render['msg'] = $Projeto->daoErrorMessage;
            }
        }
    }
    
    /**
     * @return redirect
     */
    public function excluirAction($projetoSlug)
    {
        $status = true;
        
        $Projeto = Projeto::getBySlug($projetoSlug);

        if( !$Projeto ){
            $status = false;
            _setError("O projeto informado é inválido.");
        }
        
        if( $status === true ){
            
            if( !$Projeto->save('D') ){
                $status = false;
            }
            
        }
        
        if( $status === false ){
            $this->redirect(url_module."/visualizacao/{$projetoSlug}");
        }else{
            _setSuccess("Projeto excluído com sucesso");
            $this->redirect(url_mode."/projeto/lista");
        }
        
    }
    
    /**
     * @return void
     */
    public function alterarOrdenacaoAction( $projetoSlug )
    {
        $Projeto = Projeto::getBySlug($projetoSlug);
        if( !$Projeto ){
            die("O projeto informado é inválido.");
        }
        
        $projetos = Projeto::getList(array(), null, 0, null, array('dao.ordenacao asc'));
        
        $indice = 0;
        foreach( $projetos['rows'] as $Projeto ){
            
            if( $Projeto->getSlug() === $projetoSlug ){
                    $ProjetoAntigo = $projetos['rows'][$indice-1];
                    $ProjetoAntigo->setOrdenacao($indice);
                    $ProjetoAntigo->save();
                    $Projeto->setOrdenacao($indice-1);
                    $projetos['rows'][$indice-1] = null;
            }else{
                $Projeto->setOrdenacao($indice);
            }
            
            $Projeto->save();
            $indice++;
        }
        
    }
    
    /**
     * @return void
     */
    public function alterarOrdenacaoImagensAction( $projetoSlug, $imagemID )
    {
        $Projeto = Projeto::getBySlug($projetoSlug);
        if( !$Projeto ){
            die("O projeto informado é inválido.");
        }
        
        $lista = Anexo::getList(array(
            'dao.projeto_id = ?' => $Projeto->getID(),
            'dao.tipo = ?' => 'IMG'
        ), null, 0, null, array('dao.ordenacao asc','dao.id asc'));
        
        
        $indice = 0;
        foreach( $lista['rows'] as $Imagem ){
            
            if( $Imagem->getID() === $imagemID ){
                    $ImagemAntiga = $lista['rows'][$indice-1];
                    $ImagemAntiga->setOrdenacao($indice);
                    $ImagemAntiga->save();
                    $Imagem->setOrdenacao($indice-1);
                    $lista['rows'][$indice-1] = null;
            }else{
                $Imagem->setOrdenacao($indice);
            }
            
            $Imagem->save();
            $indice++;
        }
        
    }
    
}