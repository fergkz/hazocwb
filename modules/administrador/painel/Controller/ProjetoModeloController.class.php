<?php

use Model\CWB\Projeto as Projeto;
use Model\CWB\ProjetoModelo as Modelo;

class ProjetoModeloController extends \System\MyController
{   
    /**
     * View
     */
    public function novoAction( $projetoSlug = null )
    {
        $Projeto = Projeto::getBySlug($projetoSlug);
        
        if( !$Projeto ){
            return 404;
        }
        
        $render['Projeto'] = $Projeto;
        
        $this->view('projeto-modelo/cadastro.twig')->display($render);
    }
    
    /**
     * Ajax/Json
     */
    public function criarAction( $projetoSlug = null )
    {
        $render = $this->setModeloData($projetoSlug, null);
        $this->json($render, true);
    }
    
    /**
     * View
     */
    public function edicaoAction( $projetoSlug = null, $modeloSlug = null )
    {
        $Projeto = Projeto::getBySlug($projetoSlug);
        
        if( !$Projeto ){
            return 404;
        }

        $Modelo = Modelo::getBySlug($Projeto, $modeloSlug);
        if( !$Modelo || $Modelo->getProjetoID() !== $Projeto->getID() ){
            return 404;
        }
        
        $render['Projeto'] = $Projeto;
        $render['Modelo'] = $Modelo;
        $render['imagens'] = $Modelo->listaImagens();
        
        $this->view('projeto-modelo/cadastro.twig')->display($render);
    }
    
    /**
     * Ajax/Json
     */
    public function alterarAction( $projetoSlug = null, $modeloSlug = null )
    {
        $Modelo = Modelo::getBySlug(Projeto::getBySlug($projetoSlug), $modeloSlug);
        $render = $this->setModeloData($projetoSlug, $Modelo);
        $this->json($render, true);
    }
    
    /**
     * View
     */
    public function visualizacaoAction( $projetoSlug = null, $modeloSlug = null )
    {
        $Projeto = Projeto::getBySlug($projetoSlug);

        if( !$Projeto ){
            return 404;
        }

        $Modelo = Modelo::getBySlug($Projeto, $modeloSlug);
        if( !$Modelo || $Modelo->getProjetoID() !== $Projeto->getID() ){
            return 404;
        }
        
        $render['Projeto'] = $Projeto;
        $render['Modelo'] = $Modelo;
        $render['imagens'] = $Modelo->listaImagens();

        $this->view("projeto-modelo/visualizacao.twig")->display($render);
    }
    
    
    
    /**
     * Private Manipulate
     */
    private function setModeloData($projetoSlug, $Modelo = null)
    {
        $render['status'] = false;
        
        $Projeto = Projeto::getBySlug($projetoSlug);
        
        $acao = $Modelo ? "U" : "I";
        
        if( !$Projeto ){
            $render['msg'] = "Prjeto inválido";
            return $render;
        }
        
        if( $acao  == 'U' && $Modelo->getProjetoID() !== $Projeto->getID() ){
            $render['msg'] = "O Modelo informado não pertence ao projeto informado";
            return $render;
        }
        
        if( !$this->post('titulo') ){
            $render['msg'] = "O nome do modelo deve ser informado";
        }else{
            if( $acao === 'I' ){
                $Modelo = new Modelo();
                $Modelo->setProjetoID( $Projeto->getID() );
            }

            $Modelo->setTitulo( $this->post('titulo') );
            
            if( !empty($_FILES['file']) &&  !empty($_FILES['file']['name']) && count($_FILES['file']['name']) > 0 ){
                foreach( $_FILES['file']['name'] as $ind => $name ){
                    if( $name ){
                        $Modelo->setFile( $name, $_FILES['file']['type'][$ind], $_FILES['file']['tmp_name'][$ind], $_FILES['file']['size'][$ind] );
                    }
                }
            }
            
            if( $this->post('file_excluir') && count($this->post('file_excluir')) > 0 ){
                foreach( $this->post('file_excluir') as $id ){
                    $Modelo->unsetFile($id);
                }
            }
            
            if( $Modelo->save() ){
                $render['status'] = true;
                $render['redirect'] = url."/administrador/painel/projeto-modelo/visualizacao/{$Projeto->getSlug()}/{$Modelo->getSlug()}";
            }else{
                $render['msg'] = $Modelo->daoErrorMessage;
            }
        }
        
        return $render;
    }
    
    /**
     * @return redirect
     */
    public function excluirAction($projetoSlug, $modeloSlug = null )
    {
        $status = true;
        
        $Projeto = Projeto::getBySlug($projetoSlug);

        if( !$Projeto ){
            $status = false;
            _setError("O projeto informado é inválido.");
        }

        $Modelo = Modelo::getBySlug($Projeto, $modeloSlug);
        if( !$Modelo || $Modelo->getProjetoID() !== $Projeto->getID() ){
            $status = false;
            _setError("O modelo informado é inválido.");
        }
        
        if( $status === true ){
            
            if( !$Modelo->save('D') ){
                $status = false;
            }
            
        }
        
        if( $status === false ){
            $this->redirect(url_module."/visualizacao/{$projetoSlug}/{$modeloSlug}");
        }else{
            _setSuccess("Modelo excluído com sucesso");
            $this->redirect(url_mode."/projeto/visualizacao/{$projetoSlug}");
        }
        
    }
}