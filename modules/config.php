<?php

use MVC\Session as Session;
use Model\Pagina\Bloco as Bloco;
use Model\CWB\Projeto as Projeto;

# Begin:: define url to language
$expl = explode("?", $GLOBALS['url']['request']);
foreach( array_merge($_GET, array("lang"=>"")) as $ind => $row ){
    $get[] = $ind."=".$row;
}
$GLOBALS['config']['define']['url_to_lang'] = url.$expl[0].'?'.implode("&",$get);
# End:: define url to language

if( !empty($_GET['lang']) && $_GET['lang'] ){
    Session::set('language', $_GET['lang']);
}

switch( Session::get("language") ){
    default:
    case 'pt-br':
        $GLOBALS['config']['define']['translate_file'] = 'media/translate/pt-br.ini';
        break;
    case 'eng':
        $GLOBALS['config']['define']['translate_file'] = 'media/translate/eng.ini';
        break;
    case 'es':
        $GLOBALS['config']['define']['translate_file'] = 'media/translate/es.ini';
        break;
}

function getBlooco($token)
{
    return Bloco::get($token);
}

function _getProjetos()
{
    if( empty($GLOBALS['projetos']) ){
        $GLOBALS['projetos'] = Projeto::getList(array(), null, 0, 9, array('dao.ordenacao asc'));
    }
    return $GLOBALS['projetos'];
}