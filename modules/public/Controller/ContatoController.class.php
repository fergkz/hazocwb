<?php

use Model\CWB\Contato as Contato;

class ContatoController extends \System\MyController
{
    private $Usuario;

    /**
     * View
     */
    public function indexAction()
    {
        $render['Usuario'] = $this->Usuario;
        $this->view('contato/index.twig')->display($render);
    }

    /**
     * jSon
     */
    public function enviarAction()
    {
        $Contato = new Contato();
        $Contato->setNome(            $this->post('nome') );
        $Contato->setEmail(           $this->post('email') );
        $Contato->setMensagem(        $this->post('mensagem') );
        
        if( $Contato->save() ){
            $render = array(
                "status" => true
            );
        }else{
            $render = array(
                "status" => false,
                "msg" => $Contato->daoErrorMessage
            );
        }
        
        $this->json($render);
    }
}