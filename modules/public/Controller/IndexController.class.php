<?php

class IndexController extends \System\MyController
{
    public function indexAction()
    {
        $projetos = _getProjetos();
        $this->redirect(url.'/projetos/detalhes/'.$projetos['rows'][0]->getSlug()); 
    }
}