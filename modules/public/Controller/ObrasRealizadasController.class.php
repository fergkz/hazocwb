<?php

use Model\CWB\ObraRealizada as Obra;

class ObrasRealizadasController extends \System\MyController
{
    /**
     * View
     */
    public function indexAction()
    {
        $render['obras'] = Obra::getList();
        $this->view('obras-realizadas/index.twig')->display($render);
    }
}