<?php

use Model\CWB\Projeto as Projeto;
use Model\CWB\ProjetoModelo as Modelo;
use Model\CWB\Contato as Contato;

class ProjetosController extends \System\MyController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void View
     */
    public function indexAction()
    {
        $this->view("projetos/index.twig")->display();
    }

    /**
     * @return void View
     */
    public function detalhesAction( $projetoSlug = null )
    {
        $Projeto = Projeto::getBySlug($projetoSlug);
        if( !$Projeto ){
            return 404;
        }
//        $listaImagens = $Projeto->listaImagens(500, 1);
        $listaImagens = $Projeto->listaImagens(500);
        $render['Projeto'] = $Projeto;
        $render['imagens'] = $listaImagens;
//        $render['imagem_url'] = @reset($listaImagens);
        $render['projeto_selecionado'] = $Projeto->getSlug();
        $render['projetos'] = Projeto::getList(array(), null, 0, 9, array('dao.id desc'));
        
        $render['modelos'] = Modelo::getList(array(
            'dao.projeto_id = ?' => $Projeto->getID()
        ), null , 0, null, array(
            'dao.id desc'
        ));
        
        $this->view("projetos/visualizacao.twig")->display($render);
    }
    
    public function enviarContatoAction()
    {
        $Projeto = Projeto::getBySlug($this->post('projeto'));
        
        $Contato = new Contato;
        
        $Contato->setNome(            $this->post('nome') );
        $Contato->setTelefone(        "(".$this->post('ddd').") ".$this->post('telefone') );
        $Contato->setEmail(           $this->post('email') );
        $Contato->setLocalConstrucao( $this->post('local') );
        $Contato->setMensagem(        $this->post('mensagem') );
        $Contato->setProjetoID(       $Projeto->getID() );
        
        if( $Contato->save() ){
            $render = array(
                "status" => true
            );
        }else{
            $render = array(
                "status" => false,
                "msg" => $Contato->daoErrorMessage
            );
        }
        
        $this->json($render);
    }
    
}