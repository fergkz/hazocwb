<?php

use Model\CWB\Contato as Contato;

class QuemSomosController extends \System\MyController
{
    private $Usuario;

    /**
     * View
     */
    public function indexAction()
    {
        $render['Usuario'] = $this->Usuario;
        $this->view('quem-somos/index.twig')->display($render);
    }
}